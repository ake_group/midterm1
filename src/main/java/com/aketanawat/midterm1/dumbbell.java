/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.midterm1;

/**
 *
 * @author Acer
 */
public class dumbbell {
    
    private int size;  // size คือขนาดของดัมเบล 
    private int count; // count คือจำนวนครั้งที่ยก
    

    public dumbbell(int size, int count) { //สร้าง Constructor เซต count และ size
        this.count = count;
        this.size = size;
    }

    public void times(int size, int count) {//สร้าง method times  เก็บค่า size และ count
        if (size == 3) { 
            if (count >= 20 && count <= 30) {
                System.out.println("My arms are a bit tired.");
            } else if (count > 30 && count <= 50) {
                System.out.println("My arms are very tired");
            } else if (count > 50) {
                System.out.println("My arms have no feelings");
            } else {
                System.out.println("I'm cheating");
            }
        }
        if (size == 4) {
            if (count >= 15 && count <= 25) {
                System.out.println("My arms are a bit tired.");
            } else if (count > 25 && count <= 35) {
                System.out.println("My arms are very tired");
            } else if (count > 35) {
                System.out.println("My arms have no feelings");
            } else {
                System.out.println("I'm cheating");
            }
        }
        if (size == 5) {
            if (count >= 8 && count <= 10) {
                System.out.println("My arms are a bit tired.");
            } else if (count > 10 && count <= 12) {
                System.out.println("My arms are very tired");
            } else if (count > 12) {
                System.out.println("My arms have no feelings");
            } else {
                System.out.println("I'm cheating");
            }
        }if(size !=3 && size !=4 && size !=5){
            System.out.println("There are only 3 sizes to choose from");
        }

    }

    public int getCount() { //เรียกข้อมูลจาก count โดยใช้ getCount
        return count;
    }

    public int getSize() {//เรียกข้อมูลจาก size โดยใช้ getSize
        return size;
    }

    public void setCountSize(int size, int count) { //เรียกใช้ข้อมูลทั้ง size และ count โดยใช้ setCountSize
        this.size = size;
        this.count = count;
    }

}
